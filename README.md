
## This is a to-do API example using Slim 3 and MongoDB

requirements: 

run: composer update

Can use any app like postman o send curl commands to interact with the API.

launch a php server (can use the command: php -S 127.0.0.1:8000 -t public )

ROUTES:

#### Method GET /todos : list all todos. 
(can use "page" number, completed=1, incomplete=1 or "limit" of results with get parameters: page=2&limit=5&completed=1)

#### Method GET /todos/{id}
Show a specific todo.

#### Method POST /todos 
Create a new todo with the POST [form-data] parameters.  

#### Method PUT /todos/{id} 
Update a todo with de PUT [x-www-form-urlencoded] parameters. 

#### Method DELETE /todos/{id} 
Delete a todo.


