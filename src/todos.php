<?php

/****
*
*		Gabriel Hubermann | Hubermann@gmail.com
*		to-do example using php and mongodb 
*
****/

require_once '../include/TodosHandler.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new Slim\App();

$app->add(function (Request $request, Response $response, $next) {
	$response = $next($request, $response);
	return $response
	->withHeader('Access-Control-Allow-Origin', '*')
	->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
	->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
	->withHeader('Content-Type', 'application/json');
});


#ALL
$app->get('/todos', function(Request $request, Response $response){

	$todosHandler = new TodosHandler;
	$page  = $request->getParam('page') ? (int) $request->getParam('page') : 1;
	$limit = $request->getParam('limit') ? (int) $request->getParam('limit') : 5;

	$skip  = ($page - 1) * $limit;
	$next  = ($page + 1);
	$prev  = ($page - 1);

	if($request->getParam('completed')){
		$cur = $todosHandler->getAllCompleted($skip, $limit);
		$query_status ="&completed=1";
	}elseif($request->getParam('incomplete')){
		$cur = $todosHandler->getAllIncomplete($skip, $limit);
		$query_status ="&incomplete=1";
	}else{
		$cur = $todosHandler->getAll($skip, $limit);
		$query_status ="";
	}
	$total_items = $cur['total'];

	if($page > 1){
		$link_prev = "/todos?page=" . $prev . "&limit=" . $limit.$query_status ;
		if($page * $limit < $total_items) {
			$link_next = "/todos?page=" . $next . "&limit=" . $limit.$query_status ;
		}
	} else {
		if($page * $limit < $total_items) {
			$link_next = "/todos?page=" . $next. "&limit=" . $limit.$query_status ;
		}
	}
	
	$output = [];
	$todos = [];

	foreach ($cur['items'] as $doc) {
		$tmp = [];
		$tmp["_id"] = $doc["_id"];
		$tmp["title"] = $doc["title"];
		$tmp["description"] = $doc["description"];
		$tmp["due_date"] = $doc["due_date"];
		$tmp["completed"] = $doc["completed"];
		$tmp["created_at"] = $doc["created_at"];
		$tmp["updated_at"] = $doc["created_at"];
		array_push($todos,$tmp);
	}

	$output['total_items'] = $total_items;
	$output['todos'] = $todos;
	$output['links'] = ['link_next' => $link_next, 'link_prev' => $link_prev];
	$output['limit_per_page'] = $limit;

	return $response->withJson($output, 200);

});


#CREATE
$app->post('/todos', function(Request $request, Response $response){
	$res =[];
	$res["errors"] = FALSE;

	$title = $request->getParam('title');
	$description = $request->getParam('description');
	$due_date = $request->getParam('due_date');

	if($title == ""){$res["errors"] = TRUE; $res["notice"] = "title is required";}
	if($due_date==""){$res["errors"] = TRUE; $res["notice"] = "due_date is required";}
	if(!validateDate($due_date)){$res["errors"] = TRUE; $res["notice"] = "due_date is required | format YYYY-MM-DD hh:mm:ss";}

	if($res["errors"]==FALSE){
		$todosHandler = new TodosHandler;
		$cur = $todosHandler->insert($title, $description, $due_date, 0);
		if($cur == INSERT_SUCCESS) {
			$last = $todosHandler->last();
			$res["message"] = "New todo added";
			$res["todo"] = $last;
			return $response->withJson($res, 200); 
		} else {
			$res["errors"] = TRUE;
			$res["message"] = "Failed to add a new todo";
		}
	}
	return $response->withJson($res, 201);
	
});


#SHOW
$app->get('/todos/{id}', function(Request $request, Response $response, $args){
	$todosHandler = new TodosHandler;
	$cur = $todosHandler->findOne($args['id']);
	return $response->withJson($cur, 200);
});


#UPDATE
$app->put('/todos/{id}', function(Request $request, Response $response, $args){
	$res = [];
	$res["errors"] = FALSE;

	#actual 
	$todosHandler = new TodosHandler;
	$exists = $todosHandler->findOne($args['id']);

	$title = $request->getParam('title');
	$description = $request->getParam('description'); 
	$due_date = $request->getParam('due_date');
	$completed = $request->getParam('completed');

	if($request->getParam('title') && $request->getParam('title') == ""){$res["errors"] = TRUE; $res["notice"] = "title is required";}else{$title = $exists['title'];}
	if($request->getParam('due_date') && $request->getParam('due_date') ==""){$res["errors"] = TRUE; $res["notice"] = "due_date is required";}else{$due_date = $exists['due_date'];}
	if($request->getParam('due_date') && !validateDate($due_date)){$res["errors"] = TRUE; $res["notice"] = "due_date is required | format YYYY-MM-DD hh:mm:ss";}

	if($res["errors"]==FALSE){


		if($exists != NULL){
			$cur = $todosHandler->update($args['id'],$title, $description, $due_date, $completed);
			if($cur == UPDATE_SUCCESS) {
				$res["errors"] = FALSE;
				$res["message"] = "Success on update todo";
				$res['todo'] = $cur = $todosHandler->findOne($args['id']);
				return $response->withJson($res, 200); 
			} else {
				$res["errors"] = TRUE;
				$res["message"] = "Failed to update todo";
				return $response->withJson($res, 201);
			}
		}else{
			$res["message"] = "Resource not found";
			return $response->withJson($res, 404);
		}

	}else{
		return $response->withJson($res, 201);
	}

});


#DELETE
$app->delete('/todos/{id}', function(Request $request, Response $response, $args){
	
	$res = [];
	$todosHandler = new TodosHandler;
	$exists = $todosHandler->findOne($args['id']);
	
	if($exists != NULL){
		
		$cur = $todosHandler->remove($args['id']);
		if($cur == REMOVE_SUCCESS) {
			$res["errors"] = FALSE;
			$res["message"] = "Resource successfully removed";
			return $response->withJson($res, 200); 
		}

		$res["errors"] = TRUE;
		$res["message"] = "Failed to remove todo";
		return $response->withJson($res, 201);
	}

	$res["message"] = "Resource not found";
	return $response->withJson($res, 404);	
});


function validateDate($date, $format = 'Y-m-d H:i:s')
{
	$d = DateTime::createFromFormat($format, $date);
	return $d && $d->format($format) == $date;
}
