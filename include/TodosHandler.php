<?php
/****
*
*		Gabriel Hubermann | Hubermann@gmail.com
*		to-do example using php and mongodb 
*
****/
require_once(__DIR__.'/../include/config.php');


class TodosHandler{
	
	public function __construct(){
		$conn = new MongoDB\Client("mongodb://localhost:27017");
    $this->db = $conn->todos_example->todos_collection;
	}

	public function getAll( $skip, $limit){
		$result = $this->db->find([], ['skip'=> $skip, 'limit'=>$limit]);
		$total = $this->db->count();
		return ["items" => $result, "total" => $total];
	}

	public function getAllCompleted( $skip, $limit){
		$result = $this->db->find(['completed' => "1"], ['skip'=> $skip, 'limit'=>$limit]);
		$total = $this->db->count(['completed' => "1"]);
		return ["items" => $result, "total" => $total];
	}

	public function getAllIncomplete( $skip, $limit){
		$result = $this->db->find(['completed' => 0], ['skip'=> $skip, 'limit'=>$limit]);
		$total = $this->db->count(['completed' => 0]);
		return ["items" => $result, "total" => $total];
	}

	public function total(){
		return $result = $this->db->count();
	}

	public function findOne($id){
		return $result = $this->db->findOne(['_id' => new MongoDB\BSON\ObjectID($id)]);
	}

	public function last(){
		return $this->db->findOne([], ['sort' => ['$natural' => -1]]);
	}



	public function insert($title, $description, $due_date, $completed=0){

		$created_at = new DateTime();
		$updated_at = new DateTime();
    $document = array(
      "title" => $title,
      "description" => $description,
      "due_date" => $due_date,
      "completed" => $completed,
      "created_at" => $created_at->format('Y-m-d H:i:s'),
      "updated_at" => $updated_at->format('Y-m-d H:i:s')
    );

    try {
      $cur = $this->db->insertOne($document);
      return INSERT_SUCCESS;
    }
    catch (MongoCursorException $e) {
      return INSERT_FAILED;
    }


	}

	public function remove($id){
		try {
      $this->db->deleteOne(['_id' => new MongoDB\BSON\ObjectID($id)]);
      return REMOVE_SUCCESS;
    }
    catch (MongoCursorException $e) {
      return REMOVE_FAILED;
    }
	}

	public function update($id, $title, $description, $due_date, $completed=0){

		$actual = $this->db->findOne(['_id' => new MongoDB\BSON\ObjectID($id)]);
		$updated_at = new DateTime();
		
		$newdata = [
			'_id' => new MongoDB\BSON\ObjectID($id),
      "title" => $title,
      "description" => $description,
      "completed" => $completed,
      "due_date" => $due_date,
      "created_at" => $actual["created_at"],
      "updated_at" => $updated_at->format('Y-m-d H:i:s')
    ];

		try {
      $this->db->replaceOne(['_id' => new MongoDB\BSON\ObjectID($id)], $newdata);
      return UPDATE_SUCCESS;
    }
    catch (MongoCursorException $e) {
      return UPDATE_FAILED;
    }
	}

}
?>